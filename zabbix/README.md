
### Deploying Zabbix Server
## Required Variables

These variables need to be set and exported
`export ZABBIX_SERVER_LDAP_BIND_PASSWORD`
`export ZABBIX_SERVER_DBPASSWORD`

To run in OVH:

```bash
export OS_FORCE_IPV4=true
# source openrc file from OVH
ansible-playbook provision-zabbix.yml
# Now place DNS entries in cloudflare before proceeding
ansible-playbook -i ../inventories/openstack_inventory.py deploy-galera.yml
ansible-playbook -i ../inventories/openstack_inventory.py deploy-zabbix.yml
```

To run elsewhere you must create your servers, define your inventory, and then
run the deploy-galera and deploy-zabbix playbooks


### Deploying the Zabbix Agent
The deploy-agent-zabbix.yml playbook will deploy Zabbix agent to a target host using the [zabbix_agent role](https://github.com/ansible-collections/community.zabbix/tree/main/roles/zabbix_agent) from the [community.zabbix collection](https://github.com/ansible-collections/community.zabbix).
This role will deploy the zabbix agent to several different OSes.  Please see the [zabbix_agent role](https://github.com/ansible-collections/community.zabbix/tree/main/roles/zabbix_agent) for more documentation.
